no_of_oper = 0
output = ""
def notation(num1, num2, oper):
    if oper == '+' :
        return 'A'
    elif oper == '-' :
        return 'S'
    elif oper == '*' :
        return 'M'
    elif oper == '/' :
        return 'D'

def isAlphabet(j):
    return exp[j].isalpha()

def precedingOperator(j):
    return not (j > 0 and exp[j-1].isalpha())

def succeedingDivisionOperator(j):
    return not (j+1 < len(exp) and (exp[j+1].isalpha() or exp[j+1] == '/'))

def ifAlphabet(a, b, j):
    global no_of_oper, output
    oper = notation(b, a, exp[j])
    output += 'L' + " " + b +'\n' + oper + " " + a + '\n'
    if not succeedingDivisionOperator(j) :
        no_of_oper += 1
        output += "ST" + " " + "$" + str(no_of_oper) + '\n'
    result="$" + str(no_of_oper)
    l.append(result)
    return output


def ifOperator(a, b, j):
    global no_of_oper, output
    oper = notation(b, a, exp[j])
    output += oper + " " + b + '\n'
    if b[0] != '$' :
        result = '$' + str(no_of_oper)
    else :
        result = b
        no_of_oper = int(b[1])
    l.append(result)
    if not succeedingDivisionOperator(j) :
        output += 'ST' + " " + result + '\n'
    return output

               
def ifNegation(j):
    global no_of_oper, output
    a = l.pop()
    if exp[j-1].isalpha() :
        no_of_oper += 1 
        output += 'L' + " " + a + '\n'
    output += 'N' +'\n'
     if not succeedingDivisionOperator(j) :
        if not isAlphabet(j-1) :
            no_of_oper += 1
            output += 'ST' + " " + '$' + str(no_of_oper) + '\n'
    result = '$' + str(no_of_oper)
    l.append(result)
    return output


def ifSubtract(a, b, j):

    global no_of_oper, output
    oper = notation(b, a, '+')
    output += 'N' + '\n' + oper + " " + b + '\n'
    if b[0] != '$' :
        result = '$' + str(no_of_oper)
    else:
        result = b
        no_of_oper = int (b[1])
     if not succeedingDivisionOperator(j) :
        output += 'ST' + " " + result +'\n'
     l.append(result)
     return output 


def checkOperator(a, b, j):
    if precedingOperator(j) and exp[j] == '-' :
        ifSubtract(a, b, j)
    elif precedingOperator(j) and exp[j] != '/' :
        ifOperator(a, b, j)
    else:
        ifAlphabet(a, b, j)


def codeGeneration(exp):
   global output
   for j in range(len(exp)):
     if isAlphabet(j) :
        l.append(exp[j])
     elif exp[j] != '@' :
        a = l.pop()
        b = l.pop()
        checkOperator(a, b, j)
     else:
        ifNegation(j)
   return output

exp = input("enter a postfix expression:")
l = []
print(codeGeneration(exp))
